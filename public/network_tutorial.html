<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8' />
		<meta http-equiv="X-UA-Compatible" content="chrome=1" />
		<meta name="description" content="NICTA SmartGridToolbox : Network tutorial" />
		<link rel="stylesheet" type="text/css" media="screen" href="stylesheets/stylesheet.css">
		<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
		<title>NICTA SmartGridToolbox Network Tutorial</title>
	</head>
	<body>
	<!-- MAIN CONTENT -->
		<div id="main_content_wrap" class="outer">
			<section id="main_content" class="inner">
				<p>
					<img src="images/logo.png?raw=true" alt="SmartGridToolbox Logo">
				</p>
				<h3><a name="network-tutorial" class="anchor" href="#network-tutorial"></a>Network Tutorial</h3>
				<p>
					In this tutorial, we will explore the basics of how to set up and solve models of electricity networks. We are not performing any discrete event simulations, and therefore we link only to <code>SgtCore</code>, not <code>SgtSim</code>.
				</p>
				<p>
					The tutorial can be found in the <code>tutorials/network</code> subdirectory of the SmartGridToolbox directory. Copy this directory into a convenient work directory. <code>cd</code> into the directory, run <code>./autogen.sh</code> (if needed) and <code>./configure</code>. Now <code>make</code> to build the two executables, <code>network</code> and <code>network_yaml</code>.
				</p>
				<p>
					Run the <code>network</code> executable by typing <code>./network</code>, taking note of the output. Now read through the code (<a href="https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network.cc">network.cc</a>), paying special attention to the documentation and comparing the code to the output. 
				</p>
				<p>
				Below, we discuss the code in more detail.
<pre class="prettyprint"><code class="lang-cpp">#include &ltSgtCore.h&gt // Include SgtCore headers.
</code></pre>
				SmartGridToolbox is packaged as two libraries: SgtCore, and SgtSim. SgtCore contains utility code for handling dates, time, logging, parsing, vectors and matrices, complex numbers and so on, as well as basic code for solving power flow problems. SgtSim adds a layer for performing discrete event simulations, as well as a component library of simulation objects such as batteries, PV panels, inverters and so on.
<pre class="prettyprint"><code class="lang-cpp">    using namespace Sgt;
</code></pre>
				Most SmartGridToolbox objects are in the an namespace called "<code>Sgt</code>".
<pre class="prettyprint"><code class="lang-cpp">    auto bus1 = std::make_shared&ltBus&gt("bus_1", Phase::BAL, arma::Col&ltComplex&gt{11}, 11);
        // Create a bus named "bus_1", with a single balanced (BAL) phase, a nominal voltage vector of [11 kV]
        // (with only 1 element because it is single phase), and base voltage of 11 kV. 
    bus1-&gtsetType(BusType::SL);
        // Make it the slack bus for powerflow.
    nw.addBus(bus1);
        // Add it to the network.
</code></pre>
				Here, we create a shared pointer to a new <code><a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_bus.html">Bus</a></code> object, with id "bus_1", using the parameters of the <code><a href="http://">Bus</a></code> constructor. A bus represents a node in the network. It can have one or more electrical phases, and has an associated set of voltages, one for each phase. Components such as <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html">loads</a> and <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_gen.html">generators</a> can be associated with a bus, and two buses may be connected together using a <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_branch_abc.html">branch</a>. After creating the bus, its type (for the purpose of power flow calculations) is changed from the default of  <code>BusType::PQ</code> to <code>BusType::SL</code>, making it into the special "slack" generator bus for the network. Finally, the bus is added to the network object that was created earlier. Please consult a text or tutorial on AC power flow to find out more about bus types and other aspects of the power flow problem.
<pre class="prettyprint"><code class="lang-cpp">    auto gen1 = std::make_shared&ltGen&gt("gen_1", Phase::BAL);
        // Create a generator named "gen_1".
    nw.addGen(gen1, "bus_1");
        // Add it to the network, attaching it to bus_1.
   
    auto bus2 = std::make_shared&ltBus&gt("bus_2", Phase::BAL, arma::Col&ltComplex&gt{11}, 11);
        // Another bus...
    bus2-&gtsetType(BusType::PQ);
        // Make it a PQ bus (load or fixed generation).
    nw.addBus(bus2);
        // Add it to the network.
    
    auto load2 = std::make_shared&ltZip&gt("load_2", Phase::BAL);
        // Create a "ZIP" load named load_2.
    load2-&gtsetSConst({std::polar(0.1, -5.0 * pi / 180.0)}); 
        // The load consumes a constant S component of 0.1 MW = 100 kW, at a phase angle of -5 degrees.
    nw.addZip(load2, "bus_2");
        // Add it to the network and to bus_2.
</code></pre>
				Here, we add a generator (<a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_gen.html">Gen</a>) to bus_1, create another bus named bus_2, and add a load (<a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_zip.html">Zip</a>) to bus_2, all using the same patten that was used to create and add bus_1.
<pre class="prettyprint"><code class="lang-cpp">    auto line = std::make_shared&ltCommonBranch&gt("line");
        // Make a "CommonBranch" (Matpower style) line.
    line->setYSeries({Complex(0.005, -0.05)});
        // Set the series admittance of the line.
    nw.addBranch(line, "bus_1", "bus_2");
        // Add the line to the network, between the two buses.
</code></pre>
				A line is now added, of class <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_common_branch.html">CommonBranch</a>, which derives from <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/class_sgt_1_1_branch_abc.html">BranchAbc</a>. The line connects the two buses together.
			</p>
			<p>
				At this point, our network consists of two buses, bus_1 and bus_2. bus_1 is a slack bus, with a generator attached, and bus_2 is a PQ bus, with a load attached. We may now solve the power flow problem:
<pre class="prettyprint"><code class="lang-cpp">    nw.solvePowerFlow();
</code></pre>
After doing so, we print out some information about the state of the network:
<pre class="prettyprint"><code class="lang-cpp">    // We show below how to retrieve some information:
    auto bus = nw.buses()["bus_2"];
    sgtLogMessage() << "Bus " << bus->id() << ": " << " voltage is " << bus->V() << std::endl;
        // Note logging...

    sgtLogMessage() << "Network: " << nw << std::endl;
        // Print the network.

</code></pre>	
				</p>
				<p>			
					Typically, one wouldn't explicitly code all the network components, as is done in this first example. Instead, objects would be loaded in from a YAML configuration file. <a href="https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network_yaml.cc">network_yaml.cc</a> loads the same problem as <a href="https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network.cc">network.cc</a> in from the configuration file <a href="https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/tutorials/network/network.yaml">network.yaml</a>, and again solves the network, achieving the same solution. The relevant code looks like this:
<pre class="prettyprint"><code class="lang-cpp">   Network nw;
        // Create a network named "network".
    Parser<Network> p; 
        // Make a network parser to parse information into the network.
    p.parse("network.yaml", nw);
        // Read in from the config file "network.yaml".
        
    nw.solvePowerFlow();
        // Solve the power flow problem, using default Newton-Raphson solver.
</code></pre>
					and the YAML configuration file looks like this:
<pre class="prettyprint"><code class="lang-yaml">- bus:
    id: bus_1
    phases: [BAL]
    type: SL
    V_base: 11
    V_nom: [11]
- gen:
    id: gen_1
    bus_id: bus_1
    phases: [BAL]
- bus:
    id: bus_2
    phases: [BAL]
    type: PQ
    V_base: 11
    V_nom: [11]
- zip:
    id: load_2
    bus_id: bus_2
    phases: [BAL]
    S_const: [[0.1D-5.0]] # 0.1 @ -5 degrees.
- common_branch:
    id: line
    bus_0_id: bus_1
    bus_1_id: bus_2
    Y_series: 0.005-0.05j
</code></pre>
To find out more about the YAML specification for SmartGridToolbox networks, see <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/group___network_yaml_spec.html">here</a> (this information may be incomplete). To see the API reference section relating to networks, see <a href="http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/group___power_flow_core.html">here</a>.
				</p>
			</section>
		</div>
	</body>
</html>
