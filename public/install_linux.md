[//]: # (Render using the grip python package: grip --export install_linux.md)

# Installing SmartGridToolbox on Linux
These instructions require a reasonably up to date version of Linux - for example, Ubuntu 17 (Artful Aardvark) or greater, or Debian 9 (Stretch) or greater. If you would like to install using a slightly older version of Linux, for example, Ubuntu 14 (Trusty Tahr), you will need to make special provisions to compile using a more up to date version of g++ than the default. g++-7 has been tested, and works. While we don't make any guarantees, you may be able to get an idea of how this can be done by looking at SmartGridToolbox's [`.gitlab-ci.yml`](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/.gitlab-ci.yml) file.

The instructions will get you started with a basic installation of SmartGridToolbox, without extras or extensions.

## Third Party Packages
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential
sudo apt-get install gfortran
sudo apt-get install cmake
sudo apt-get install autoconf 
sudo apt-get install automake 
sudo apt-get install libtool 
sudo apt-get install libboost-all-dev
sudo apt-get install libblas-dev
sudo apt-get install liblapack-dev
```

## SmartGridToolbox

### Obtain the SmartGridToolbox source
```
git clone https://gitlab.com/SmartGridToolbox/SmartGridToolbox.git
cd SmartGridToolbox
git submodule init
git submodule update
```

### Build and install third party dependencies
```
cd third_party
./install_third_party.sh g++ /usr/local
cd ..
```

### Build and install SmartGridToolbox
```
./autogen.sh
./configure
make -j4
sudo make install
```

## Compiling and linking your executable
Please see [here](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/compiling_and_linking.html) for instructions about how to compile and link your executable with SmartGridToolbox.

## Setting your LD\_LIBRARY\_PATH
You may need to make sure the `LD\LIBRARY\_PATH` environment variable is set to include `/usr/local/lib` to avoid dynamic linking errors when running SmartGridToolbox programs. For example:
```
export LD_LIBRARY_PATH=/usr/local/lib
```
You can add this line to your `.bash_profile` or equivalent to make this change permanent.
