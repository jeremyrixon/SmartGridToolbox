# SmartGridToolbox

[![pipeline status](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/badges/master/pipeline.svg)](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/commits/master)

[SmartGridToolbox](http://smartgridtoolbox.gitlab.io/SmartGridToolbox) is a C++14 library for electricity grids and associated elements, concentrating on smart/future grids and grid optimisation. It is designed to provide an extensible and flexible starting point for developing a wide variety of smart grid simulations.

## Getting Started

Please see the build instructions for [Linux](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/install_linux.html) and [MacOS](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/install_macos.html). The API reference is available [here](http://smartgridtoolbox.gitlab.io/SmartGridToolbox/doxygen_html/index.html). Additional information and documentation is available at the [SmartGridToolbox homepage](http://smartgridtoolbox.gitlab.io/SmartGridToolbox).

## License

SmartGridToolbox is licensed under the Apache 2.0 License. Please see the [LICENSE](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/LICENSE) file for details. Also see the [NOTICE](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/blob/master/NOTICE) file (which must be redistributed with derivative works) for details about how to cite useage.
